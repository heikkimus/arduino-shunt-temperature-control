

/*
LCD  Arduino
	PIN1 = GND
	PIN2 = 5V
	RS(CS) = 8;
	RW(SID)= 9; 
	EN(CLK) = 3;
	PIN15 PSB = GND;
*/
#include "OneWire.h"
#include "U8glib.h"
#include "temperatureSensor.h"

#include <stdio.h>
#include <float.h>
#include <StackArray.h>
#include <Servo.h> 



U8GLIB_ST7920_128X64 u8g(3, 9, 8, U8G_PIN_NONE);                  // SPI Com: SCK = en = 18, MOSI = rw = 16, CS = di = 17

const int potentiometerPin = A0; //Analog 0
const int tmp36Pin = A1; //Analog 0
const int buttonMenu   = 0;     // the number of the pushbutton pin
//const int buttonPlus   = 1;     // the number of the pushbutton pin
//const int buttonMinus  = 2;     // the number of the pushbutton pin
//LCD Clock            = 3;     // the number of the pushbutton pin
//const int buttonNext   = 4;     // the number of the pushbutton pin
const int servoPin     = 5;     // the number of the pushbutton pin
//LCD RW                 8
//LCD CS                 9
const int lcdLight = 12;

//Prototypes
void readButtons();
void renderRoomtemperatureScreen();
void renderShuntScreen();
void renderServoTest();
void adjustShunt(float wantedTemperature, int timeReference, float temperatureArray[106]);
int interpretPotValuesToTemperature();
float getAnalogTemperature();

//Globals
int iScreen = 0; //0 = Overall screen, 1 = History screen, 2 = Settings screen
int buttonState = 0;
byte sensor1Addr[8] = {0x28, 0xA3, 0x80, 0x18, 0x4, 0x0 , 0x0, 0x6A};
byte sensor2Addr[8] = {0x28, 0xB6 ,0x9F ,0x0 ,0x4 ,0x0, 0x0, 0xED};
TemperatureSensor shunt(sensor1Addr); //Suntti
TemperatureSensor reservoir(sensor2Addr); //Varaja
int iRunner = 32;//Used for  running text on scre
float reservoirTemperatures[106];//nbr of pixels
float shuntTemperatures[106];//nbr of pixels
float indoorTemperatures[106];//nbr of pixels
Servo myservo;
int pastTime;

const float reservoirTmpCorrection = 18;
float wantedTemperature; //For shunt
int measuringInterval = 68; //seconds


void setup(){
  Serial.begin(9600); 
  Serial.println("Init");

  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) 
     u8g.setColorIndex(255);     // white
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
     u8g.setColorIndex(3);         // max intensity
  else if ( u8g.getMode() == U8G_MODE_BW )
     u8g.setColorIndex(1);         // pixel on

  pinMode(lcdLight, OUTPUT);
  analogWrite(lcdLight, 128);
  pinMode(buttonMenu, INPUT); 

  wantedTemperature = 60;
  pastTime = millis()/1000;
  }



void loop(){  
  if (digitalRead(buttonMenu) == LOW) {     //Menu pressed, go to next screen
     Serial.println("Nappi painettu");
     delay(100);
     iScreen ++;
  	 if(iScreen>1) iScreen = 0;//Only two screens available atm
  }

  if(((millis()/1000)-pastTime)>measuringInterval){//refresh temperature sensors according measuring intervals
    for (int j = 1; j < 106; j++){ //Pushing the newest value to the end of the array
		  reservoirTemperatures[j-1] = reservoirTemperatures[j];
		  shuntTemperatures[j-1] = shuntTemperatures[j];
      indoorTemperatures[j-1] = indoorTemperatures[j];
    }
   reservoirTemperatures[105]  = reservoirTmpCorrection + reservoir.getTemperature();
   shuntTemperatures[105]      = shunt.getTemperature();
   indoorTemperatures[105]     = getAnalogTemperature();
   pastTime = millis()/1000;
  	//adjustShunt(indoorTmp);
  }

int wantedTemperature = interpretPotValuesToTemperature();
u8g.firstPage();  
do {
  switch(iScreen){
    case 0:{
      adjustShunt(wantedTemperature, 5, shuntTemperatures);
      renderShuntScreen();
      break;
    }
    case 1:{
      adjustShunt(wantedTemperature, 26, indoorTemperatures); //Timewindow of 30 mins
      renderRoomtemperatureScreen();
      break;
    }
    case 2:{
      renderServoTest();
      break;
    }
    default:{
      renderRoomtemperatureScreen();
      break;
    }
  }
} while( u8g.nextPage() );
}

void renderShuntScreen(){
  /*
   * Renders screen, which allows to controll shunt's temperature
  */
    int boxX = 22;
    int boxY = 33;
    int boxHeight = 31;
    int boxWidth = 107;
    int multiplier = 60/((measuringInterval*15)/60);//60mins/((measuringinterval*105-90)/60s)For calculating the temperaturedifference in hour
    float tempDifference = 0;
    float minimum=reservoirTemperatures[105], maximum=reservoirTemperatures[105];
    float oldRange = 0;
    float newRange = 30; //height of the frame for histogram

    u8g.setFont(u8g_font_04b_03);
    u8g.setColorIndex(1);
    u8g.setPrintPos(25,5);
    u8g.print("Varaaja / Suntti");

    u8g.setFont(u8g_font_fub14);
    u8g.setPrintPos(10,23);
    u8g.print(reservoirTemperatures[105]); u8g.print("/"); u8g.print(shuntTemperatures[105]);

    u8g.setFont(u8g_font_04b_03);
    u8g.print("(" + String((int)wantedTemperature) + ")");

    u8g.drawFrame(boxX, boxY, boxWidth, boxHeight);//void U8GLIB::drawFrame(u8g_uint_t x, u8g_uint_t y, u8g_uint_t w, u8g_uint_t h)

    for(int i = 0; i<106; i++){
      if(0!=reservoirTemperatures[i])minimum = min(reservoirTemperatures[i], minimum);
      maximum = max(reservoirTemperatures[i], maximum);
    }

    //Scaling the number for the range
    oldRange = (maximum - minimum);

    for(int i = 0; i<106; i++){ //Histogram
      if(0!=reservoirTemperatures[i]){
       float newValue = (((reservoirTemperatures[i] - minimum) * newRange) / oldRange);
  	   u8g.drawPixel(i+boxX, 63 - (int)newValue);//u8g_uint_t x, u8g_uint_t y, u8g_uint_t h       
      }
    }

    tempDifference = reservoirTemperatures[105]-reservoirTemperatures[90];

    u8g.setPrintPos(0,32);
    u8g.print(maximum); u8g.print("C");

    u8g.setPrintPos(0,64);
    u8g.print(minimum); u8g.print("C");

    u8g.setPrintPos(58,42);
    u8g.print("120 min");

    u8g.setPrintPos(58,63);
    u8g.print(tempDifference*multiplier); u8g.print("C/h ");
    u8g.print(tempDifference*2.32); u8g.print("kWh"); //1C of 2000L reservoir contains 2.32kWh http://www.puulammitys.info/index.php?topic=106.0
}

int interpretPotValuesToTemperature(){
/*Translates the potentiometer value to temperature. This will set the value for the wanted shunt temperature
*/
  float temperature;
  int potVal = analogRead(potentiometerPin);

  //Scaling the number for the range
  float oldRange = 1023; //Analog read resolution
  float newRange = 90; //40-90 Celcius

  //Scaling the pot-value for wanter temp-range
  temperature = (potVal * newRange) / oldRange;
  return (int)temperature;
}

void readButtons(){
  //Reads the state of the menu button and changes screen accordingly
  if (digitalRead(buttonMenu) == LOW) {     //Menu pressed, go to next screen
   Serial.println("Nappi painettu");
   delay(100);
   if(iScreen > 2){
     iScreen = 0;
  }
  else iScreen++;
  }
}


void renderServoTest(){
  int newServoPosition = 0;

  u8g.setFont(u8g_font_fub14);
  u8g.setPrintPos(10,23);
    
  if(iRunner == 1){
   newServoPosition=360;
   u8g.print("Servotesti, Servo auki");
   iRunner = 0;
  }
  else {
  newServoPosition = 0;
  u8g.print("Servotesti: Servo kiinni");
  iRunner = 1;
  }
  myservo.attach(servoPin);
  myservo.write(newServoPosition);
  delay(1500);
  myservo.detach();
}

void renderRoomtemperatureScreen(){
    int boxX = 22;
    int boxY = 33;
    int boxHeight = 31;
    int boxWidth = 107;
    int multiplier = 60/((measuringInterval*15)/60);//60mins/((measuringinterval*105-90)/60s)For calculating the temperaturedifference in hour
    float tempDifference = 0;
    float minimum=indoorTemperatures[105], maximum=indoorTemperatures[105];
    float oldRange = 0;
    float newRange = 30; //height of the frame for histogram

    u8g.setFont(u8g_font_04b_03);
    u8g.setColorIndex(1);
    u8g.setPrintPos(25,5);
    u8g.print("Sisa / Varaaja");

    u8g.setFont(u8g_font_fub14);
    u8g.setPrintPos(10,23);
    u8g.print(indoorTemperatures[105]); u8g.print("/"); u8g.print(reservoirTemperatures[105]);

    u8g.setFont(u8g_font_04b_03);
    u8g.print("(" + String((int)wantedTemperature) + ")");

    u8g.drawFrame(boxX, boxY, boxWidth, boxHeight);//void U8GLIB::drawFrame(u8g_uint_t x, u8g_uint_t y, u8g_uint_t w, u8g_uint_t h)

    for(int i = 0; i<106; i++){
      if(0!=indoorTemperatures[i])minimum = min(indoorTemperatures[i], minimum);
      maximum = max(indoorTemperatures[i], maximum);
    }

    //Scaling the number for the range
    oldRange = (maximum - minimum);

    for(int i = 0; i<106; i++){ //Histogram
      if(0!=indoorTemperatures[i]){
       float newValue = (((indoorTemperatures[i] - minimum) * newRange) / oldRange);
       u8g.drawPixel(i+boxX, 63 - (int)newValue);//u8g_uint_t x, u8g_uint_t y, u8g_uint_t h       
      }
    }

    tempDifference = indoorTemperatures[105]-indoorTemperatures[90];
    
    u8g.setPrintPos(0,32);
    u8g.print(maximum); u8g.print("C");

    u8g.setPrintPos(0,64);
    u8g.print(minimum); u8g.print("C");

    u8g.setPrintPos(58,42);
    u8g.print("120 min");

    u8g.setPrintPos(58,63);
    u8g.print(tempDifference*multiplier); u8g.print("C/h ");
    u8g.print(tempDifference*2.32); u8g.print("kWh"); //1C of 2000L reservoir contains 2.32kWh http://www.puulammitys.info/index.php?topic=106.0
}


void adjustShunt(float wantedTemperature, int timeReference, float temperatureArray[106]){
  /*
   * Wanted temperature is the temperature what we're trying to reach
   * timeReference = 105 -> 0, this will define in how long time period will be used for adjusting the temperature controll window
   */
  float lastTemp = temperatureArray[105];
  int lastServoPosition = myservo.read();
  
  int servoMax = 180;
  int servoMin = 0;
  float dY = temperatureArray[105]-temperatureArray[timeReference];   //delta temperature
  float dYf = dY + temperatureArray[105];  //projected temperaturedifference
  int newServoPosition = 0;

  Serial.println("wantedTemperature");
  Serial.println(wantedTemperature);
  Serial.println("temp in future:");
  Serial.println(dYf);
  int step = abs(wantedTemperature - lastTemp); //Dynamic amount of movement for servo
  
  if(dYf > wantedTemperature) newServoPosition = lastServoPosition - dY ;//Slow down the heting if it will pass the wanted temperature in next 5 cycles
  else newServoPosition = lastServoPosition+dY; //Speed up the heating cycle

  if(newServoPosition > servoMin && newServoPosition < servoMax){
   myservo.attach(servoPin);
   myservo.write(newServoPosition);
   delay(600);
   myservo.detach();
  }
else{
	Serial.println("Servo is its limit position, can't adjust more");
	Serial.println(lastServoPosition);
  }
}

float getAnalogTemperature(){
  float celcius;
  //getting the voltage reading from the temperature sensor
  int reading = analogRead(tmp36Pin);  

 // converting that reading to voltage, for 3.3v arduino use 3.3
  float voltage = reading * 5.0;
  voltage /= 1024.0; 


 // now print out the temperature
 celcius = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
											   //to degrees ((volatge - 500mV) times 100)
 //delay(1000);
 return celcius;
}