
#include "TemperatureSensor.h"


TemperatureSensor::TemperatureSensor(byte address[]){	
	if (OneWire::crc8(address, 7) != address[7]) {
      //Serial.println("CRC is not valid!");
  	}
  	else{
  		for(int i =0; i<8; i++) {iAddr[i] = address[i];}
  	}
    Serial.begin(9600); 

}



float TemperatureSensor::getTemperature(){
  OneWire ds(PIN);  // on pin 13
  byte data[12];
  byte type_s;

  ds.reset();
  ds.select(iAddr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end
  
  delay(750);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  ds.reset();
  ds.select(iAddr);    
  ds.write(0xBE);         // Read Scratchpad

  for (int i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }

  // convert the data to actual temperature

  int raw = (data[1] << 8) | data[0];

  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // count remain gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } 
  else {
    unsigned char t_mask[4] = {0x7, 0x3, 0x1, 0x0};
    byte cfg = (data[4] & 0x60) >> 5;
    raw &= ~t_mask[cfg];
    }
  
  float celcius = (float)raw / 16.0;
  Serial.println("TemperatureSensor.cpp:");
  Serial.println(celcius);
  return celcius;
}

