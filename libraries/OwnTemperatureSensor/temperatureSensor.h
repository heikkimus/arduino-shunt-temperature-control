

// OneWire DS18S20, DS18B20, DS1822 Temperature Example
//
// http://www.pjrc.com/teensy/td_libs_OneWire.html
//
// The DallasTemperature library can do all this work for you!
// http://milesburton.com/Dallas_Temperature_Control_Library



// OneWire DS18S20, DS18B20, DS1822 Temperature Example
//
// http://www.pjrc.com/teensy/td_libs_OneWire.html
//
// The DallasTemperature library can do all this work for you!
// http://milesburton.com/Dallas_Temperature_Control_Library



#ifndef TemperatureSensor_h
#define TemperatureSensor_h
#include "OneWire.h"
const int PIN = 13; //Tempsensor pin


class TemperatureSensor{
	public:
		TemperatureSensor(byte address[]);
	    float getTemperature();
	private:
    	byte iAddr[8];
    	char iName;
};

#endif